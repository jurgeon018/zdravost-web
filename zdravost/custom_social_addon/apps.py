from django.apps import AppConfig


class CustomSocialAddonConfig(AppConfig):
    name = 'custom_social_addon'
