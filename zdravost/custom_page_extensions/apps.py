from django.apps import AppConfig


class CustomPageExtensionsConfig(AppConfig):
    name = 'custom_page_extensions'
